---
title: "Pa Soñjan" 
slug: "pa-sonjan"
draft: false
menu: "mainmenu"
---

Erwan Potier: Bet ganet e Dinan e Breizh. Skeudennaouer, Diorroer.

Labourat a ran gant pleustriñ reiñ gant Grigori Grabovoi. Gallout a rit prouadiñ e benveg, ar Prk1u, hag kavout a rit muioc'h ditouroù:
 - lec'hienn evit gwelet ar [Prk1u](https://www.med.grabovoi.tech/online-en#!/tab/169438428-6); 
 - Kavout a rit Titouroù e chomlerc'h:[Kinnig ar Prk1u](https://prk-1u.com/prk-1u/evolution-accelerator/?lang=fr);
 - [Kinnig Grigori Grabovoi](https://prk-1u.com/grigori-grabovoi/approche-scientifique/?lang=fr).



Deskin a ran ar Brezhonneg gant [ankiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=fr) war ma Android Pellgomz hag pleustriñ a ran gant [Assimil](https://www.assimil.com/fr/e-methodes/1173-e-methode-breton-9782700563610.html) skrivet gant Divi Karvela.


Trugarez!