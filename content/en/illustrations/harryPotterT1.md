---
title: "Harry Potter And The Philosopher's Stone" 
slug: "harry-potter-philosopher-stone"
date: 2020-09-09
draft: false
image: "HarryPotter-T1_web.png"
categories: [ 'illustrations']
tags: [ 'Harry Potter', 'Book Cover', "Color"]
---