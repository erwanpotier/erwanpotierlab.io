---
title: "Harry Potter And The Mad Bludger" 
slug: "harry-potter-mad-bludger"
date: 2020-09-09
draft: false
image: "harryPotterBludger_web.png"
categories: [ 'illustrations']
tags: [ 'Harry Potter', 'Book Cover', "Color"]
---