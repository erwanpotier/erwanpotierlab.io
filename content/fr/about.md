---
title: "A propos" 
slug: "a-propos"
draft: false
menu: "mainmenu"
---




Erwan Potier né à Dinan en Bretagne. Illustrateur. 

Certaines techniques de visualisations, comme d'autre utilisé m'ont été transmisent via des enseignements de Grigori Grabovoi. Vous pouvez d'ailleurs tester son dispositif Prk1u, l'une de ses technologies et trouver des renseignements complémentaire à ces adresses:
 - site de diffusion vidéo du [Prk1u](https://www.med.grabovoi.tech/online-en#!/tab/169438428-6); 
 - Vous trouverez également des explications sur son fonctionnement à cette adresse:[presentation du Prk1u](https://prk-1u.com/prk-1u/evolution-accelerator/?lang=fr);
 - [Présentation de Grigori Grabovoi](https://prk-1u.com/grigori-grabovoi/approche-scientifique/?lang=fr).

Pour créer un site multilingue comme celui-ci, j'ai utilisé le framework [Hugo](https://gohugo.io/) qui permet une gestion par dossier des traductions. Le site est hébergé sur [Gitlab](https://about.gitlab.com/).

Pour m'initier au breton j'ai utilisé l'application [ankiDroid](https://play.google.com/store/apps/details?id=com.ichi2.anki&hl=fr) ainsi que le méthode [Assimil](https://www.assimil.com/fr/e-methodes/1173-e-methode-breton-9782700563610.html) publié par divi Karvela.

Si vous avez un projet à me proposer vous pouvez me contacter via le formulaire de contact du site.

Merci à vous!