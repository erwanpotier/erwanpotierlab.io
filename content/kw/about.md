---
title: "A-Dro" 
slug: "a-dro"
draft: false
menu: "mainmenu"
---


Erwan Potier born in Dinan in Britany. Lymner ov.

I use some visualisations methods to manage my works, this methods are provides by Grigori Grabovoi. You can try his device, the Prk1u, you also can found some informations by following these links:
 - video diffusion of the [Prk1u](https://www.med.grabovoi.tech/online-en#!/tab/169438428-6); 
 - Some information on the behaviour of the device:[Prk1u presentation](https://prk-1u.com/);
 - [Presentation of Grigori Grabovoi](https://prk-1u.com/grigori-grabovoi/scientific-approach/).

In order to create a multilingual website, I used the framework [Hugo](https://gohugo.io/) which allow user to manage his translation by folder. I also use [Gitlab](https://about.gitlab.com/) to host my website for free.


Thanks!