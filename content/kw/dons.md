---
title: "Gwerthji" 
slug: "gwerthji"
draft: false
menu: "mainmenu"
---



Rag ow gweres godriga ma **[Redbubble gwerthji](https://www.redbubble.com/people/erwanpotier/shop)**.

***


Mars yw dha Cryptofriendly ty a yll gweres vy gans an trigva:

> Etherum: 0x147eb6Cac995436C907E4697800775158D2d3cEc
>
> Bitcoin: 0x1Puo2NBok1TPvkjXWwArPpUFfbE9d42Adp
> 
> Ripple: 0xrGxWedWvk4zL8BLxngCB3gdEcZSE6m9Jo5
>
> Chainlink: 0x147eb6Cac995436C907E4697800775158D2d3cEc
> 
> Litecoin: 0xLgH26LLw2cAnxU7xqR8b4uYzELeDeaqTyR


Meur ras a’th argevro!